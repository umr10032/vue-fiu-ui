#vue-fiu-ui

<p align="center">
    <a href="https://www.npmjs.com/package/vue-fiu-ui"><img src="https://img.shields.io/npm/dm/vue-fiu-ui.svg" alt="Downloads"></a>
  <a href="https://www.npmjs.com/package/vue-fiu-ui"><img src="https://img.shields.io/npm/v/vue-fiu-ui.svg" alt="Version"></a>
  <a href="https://www.npmjs.com/package/vue-fiu-ui"><img src="https://img.shields.io/npm/l/vue-fiu-ui.svg" alt="License"></a>
  <br>
</p>

## Intro

Vue.js is a library for building interactive web interfaces. It provides data-reactive components with a simple and flexible API. Core features include:

- [Declarative rendering with a plain JavaScript object based reactivity system](https://vuejs.org/guide/index.html#Declarative-Rendering)
- [Component-oriented development style with tooling support](https://vuejs.org/guide/index.html#Composing-with-Components)
- Lean and extensible core
- [Flexible transition effect system](https://vuejs.org/guide/transitions.html)
- Fast without the need for complex optimization

Note that Vue.js only supports [ES5-compliant browsers](http://kangax.github.io/compat-table/es5/) (IE8 and below are not supported).

## Documentation

To check out live examples and docs, visit [vuejs.org](https://vuejs.org).

## Questions

For questions and support please use the [Gitter chat room](https://gitter.im/vuejs/vue) or [the official forum](http://forum.vuejs.org). The issue list of this repo is **exclusively** for bug reports and feature requests.

## Issues

Please make sure to read the [Issue Reporting Checklist](https://github.com/vuejs/vue/blob/dev/.github/CONTRIBUTING.md#issue-reporting-guidelines) before opening an issue. Issues not conforming to the guidelines may be closed immediately.

## Contribution

Please make sure to read the [Contributing Guide](https://github.com/vuejs/vue/blob/dev/.github/CONTRIBUTING.md) before making a pull request. If you have a Vue-related project/component/tool, add it with a pull-request to [this curated list](https://github.com/vuejs/awesome-vue)!

## Changelog

Details changes for each release are documented in the [release notes](https://github.com/vuejs/vue/releases).

## Stay In Touch

For the latest releases and announcements, follow on Twitter: [@vuejs](https://twitter.com/vuejs)

## License

[MIT](http://opensource.org/licenses/MIT)

Copyright (c) 2013-present, Yuxi (Evan) You