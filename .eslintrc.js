// http://eslint.org/docs/user-guide/configuring

module.exports = {
    root: true,
    parser: 'babel-eslint',
    parserOptions: {
        sourceType: 'module'
    },
    env: {
        browser: true
    },
    // https://github.com/feross/standard/blob/master/RULES.md#javascript-standard-style
    extends: 'standard',
    // required to lint *.vue files
    plugins: [
        'html'
    ],
    // add your custom rules here
    'rules': {
        'arrow-parens': 0, // allow paren-less arrow functions
        'quotes': [2, 'single'],
        'brace-style': [2, '1tbs', {'allowSingleLine': true}], // 强制使用一致的缩进 第二个参数为 "tab" 时，会使用tab，
        'no-mixed-spaces-and-tabs': [2, false], // 不允许空格和 tab 混合缩进
        'semi': [2, 'always'], // 语句强制分号结尾
        'generator-star-spacing': 0, // allow async-await
        'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0, // allow debugger during development
        'indent': ['off', 'tab'],
        'no-tabs': 'off',
        'no-trailing-spaces': 'off'
    }
};
