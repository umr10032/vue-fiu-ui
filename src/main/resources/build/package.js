// https://github.com/shelljs/shelljs
require('./check-versions')();
require('shelljs/global');
env.NODE_ENV = 'production';

const path = require('path');
const config = require('../config');
const ora = require('ora');
const webpack = require('webpack');
const webpackProdConfig = require('./webpack.package.prod.conf');
const webpackDevConfig = require('./webpack.package.dev.conf');

console.log(
	'  Tip:\n' +
	'  Built files are meant to be served over an HTTP server.\n' +
	'  Opening index.html over file:// won\'t work.\n'
);

const spinner = ora('building for production...');
spinner.start();

webpack(webpackDevConfig, function (err, stats) {
	spinner.stop();
	if (err) throw err;
	process.stdout.write(stats.toString({
			colors: true,
			modules: false,
			children: false,
			chunks: false,
			chunkModules: false
		}) + '\n');
});

webpack(webpackProdConfig, function (err, stats) {
	spinner.stop();
	if (err) throw err;
	process.stdout.write(stats.toString({
			colors: true,
			modules: false,
			children: false,
			chunks: false,
			chunkModules: false
		}) + '\n');
});
