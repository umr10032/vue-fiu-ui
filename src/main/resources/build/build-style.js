/**
 * Created by YongX on 2017/6/15.
 */
const gulp = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('sass', function () {
	return gulp.src('../../js/sass/app.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer({
		browsers: ['last 2 versions', 'ie > 8']
	}))
	.pipe(rename('vue-fiu-ui.css'))
	.pipe(gulp.dest('../../../../dist/static/css'));
});

gulp.task('sass.min', function () {
	return gulp.src('../../js/sass/app.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer({
		browsers: ['last 2 versions', 'ie > 8']
	}))
	.pipe(cleanCSS())
	.pipe(rename('vue-fiu-ui.min.css'))
	.pipe(gulp.dest('../../../../dist/static/css'));
});

gulp.task('fonts', function () {
	gulp.src('../../../../node_modules/font-awesome/fonts/*.*')
	.pipe(gulp.dest('../../../../dist/static/fonts'));
});

gulp.task('default', ['sass', 'sass.min', 'fonts']);
