/* eslint-disable no-undef */
/**
 * version: 0.0.2-bate
 *
 * element.js
 *
 * 简单的dom元素操作
 * 作者： 黄永宣 <754060604@qq.com>
 *
 * @param name  Object | String 查询dom元素的name
 * @param root Object 默认document 父级元素对象
 * @returns Object
 */

import './vendor/element-event';

window.$ = function (name, root) {
    let el;
    let sc = document;
    if (root instanceof EM) {
        sc = root.object;
    }
    if (typeof name === 'string') {
        el = sc.querySelectorAll(name);
        if (el.length > 0) {
            if (el.length === 1) {
                el = new EM(el[0]);
            } else {
                let arr = [];
                for (let i = 0; i < el.length; i++) {
                    arr[i] = new EM(el[i]);
                }
                el = arr;
            }
        } else {
            el = null;
        }
    } else if (name instanceof Object || typeof name === 'object') {
        el = new EM(name);
    }
    return el;
};
$.isEmpty = function (str) {
    return str.length <= 0 || str === '' || str === undefined || str === null;
};
const browserType = {
    Firefox: 'Firefox',
    Chrome: 'Chrome',
    Safari: 'Safari',
    IE11: 'compatible; MSIE 11.0;',
    IE10: 'compatible; MSIE 10.0;',
    IE9: 'compatible; MSIE 9.0;',
    IE8: 'compatible; MSIE 8.0;',
    IE7: 'compatible; MSIE 7.0;',
    IE6: 'compatible; MSIE 6.0;',
    Edge: 'Trident/7.0;',
    Other: ''
};
$.browser = function () {
    let userAgent = navigator.userAgent;
    for (let key in browserType) {
        if (userAgent.indexOf(browserType[key]) !== -1) {
            return key;
        }
    }
};
$.loserIE = function () {
    let b = $.browser();
    let reg = new RegExp('IE(\\d+)');
    reg.test(b);
    let version = parseInt(RegExp['$1']);
    return version <= 8;
};

function EM (object) {
    this.object = object;
    this.style = object.style;
    if (this.object !== document) {
        this.defaultStyle = document.defaultView.getComputedStyle(object, null);
    }
}

EM.prototype.css = function (key, value) {
    if (value === undefined) {
        return this.object.style[key];
    }
    this.object.style[key] = value;
};

EM.prototype.ready = function (callback) {
    if (this.object === document) {
        if (this.object.addEventListener) {
            this.object.addEventListener('DOMContentLoaded', callback);
        } else {
            this.object.attachEvent('onreadystatechange', callback);
        }
    }
};

EM.prototype.width = function (value) {
    if (value === undefined) {
        return this.object.offsetWidth;
    }
    this.css('width', value + 'px');
};

EM.prototype.height = function (value) {
    if (value === undefined) {
        return this.object.offsetHeight;
    }
    this.css('height', value + 'px');
};

EM.prototype.bind = function (event, callback) {
    let o = this;
    if (this.object.addEventListener !== undefined) {
        this.object.addEventListener(event, function (e) {
            e.$el = o;
            callback(new ElementEvent(e));
        });
    } else {
        this.object.attachEvent('on' + event, function (e) {
            e.$el = o;
            callback(new ElementEvent(e));
        });
    }
};

EM.prototype.unbind = function (event, callback) {
    if (this.object.removeEventListener) {
        this.object.removeEventListener(event, callback);
    } else {
        this.object.detachEvent('on' + event, callback);
    }
};

EM.prototype.on = function (e, c) {
    this.bind(e, c);
};

EM.prototype.param = function (key, value) {
    if (value === undefined) {
        return this.object[key];
    }
    this.object[key] = value;
};

EM.prototype.text = function (value) {
    return this.param('innerText', value);
};

EM.prototype.html = function (value) {
    return this.param('innerHTML', value);
};

EM.prototype.val = function (value) {
    return this.param('value', value);
};

EM.prototype.hidden = function () {
    this.css('display', 'none');
};

EM.prototype.show = function () {
    this.css('display', 'block');
};

EM.prototype.animate = function (styleObject, timeout, fn, bs) {
    let style = {};
    let defStyle = {};
    bs = bs || 10;
    let index = timeout / bs;
    for (let i in styleObject) {
        style[i] = {
            val: parseInt(styleObject[i].replace(/[^-0-9]/ig, '')),
            unit: styleObject[i].replace(/[^A-Za-z]/ig, '')
        };
        defStyle[i] = {
            val: parseInt(this.defaultStyle[i].replace(/[^-0-9]/ig, '')),
            unit: this.defaultStyle[i].replace(/[^A-Za-z]/ig, '')
        };
        style[i].speed = Math.round((style[i].val - defStyle[i].val) / index);
    }
    let el = this;
    let time = setInterval(function () {
        index--;
        if (index <= 0) {
            clearInterval(time);
            for (let i in style) {
                el.css(i, style[i].val + style[i].unit);
            }
        } else {
            for (let i in style) {
                defStyle[i].val = defStyle[i].val + style[i].speed;
                el.css(i, defStyle[i].val + style[i].unit);
            }
        }
    }, bs);
};

EM.prototype.left = function (val) {
    if (val !== undefined) {
        this.css('left', val + 'px');
    }
    return this.object.offsetLeft;
};

EM.prototype.top = function (val) {
    if (val !== undefined) {
        this.css('top', val + 'px');
    }
    return this.object.offsetTop;
};
