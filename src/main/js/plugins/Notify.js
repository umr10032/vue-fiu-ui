/* eslint-disable no-undef */
export default {
    open (params) {
        let body = $('body');
        let fixed = params.fixed;
        if (fixed === undefined) {
            fixed = 'top-right';
        }
        let type = params.type;
        let content = params.content;
        let id = 'notify_div' + fixed;
        let div = $(`#${id}`);
        if (div.length === 0) {
            body.append(`<div class="notifications ${fixed}" id="${id}"></div>`);
            div = $(`#${id}`);
        }
        let nId = 'notify_id_' + this._getRandomString(12);
        let title = params.title !== undefined ? params.title : '通知';
        div.append(`<div class="alert p-10 alert-${type} animated bounceInDown" id="${nId}">
    <a href="#" class="close pull-right" data-dismiss="alert">&times;</a>
    <h4>${title}</h4>
    <i class="fa p-r-5 ${this.getIconClass(type)}"></i>
    ${content}
</div>`);
        if (params.timeout > 0) {
            this.deleteElByTimeOut(nId, params.timeout);
        } else if (params.timeout === undefined) {
            this.deleteElByTimeOut(nId, 4500);
        }
    },
    _getRandomString (len) {
        len = len || 32;
        let $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
        let maxPos = $chars.length;
        let pwd = '';
        for (let i = 0; i < len; i++) {
            pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
        }
        return pwd;
    },
    deleteElByTimeOut (id, time) {
        let el = $(`#${id}`);
        setTimeout(function () {
            el.removeClass('animated bounceInDown');
            el.addClass('animated bounceOutUp');
        }, time);

        setTimeout(function () {
            el.remove();
        }, time + 300);
    },
    getIconClass (type) {
        switch (type) {
            case 'success':
                return 'fa-check';
            case 'info':
                return 'fa-info-circle';
            case 'warning':
                return 'fa-warning';
            case 'primary':
                return 'fa-bell-o';
            case 'danger':
                return 'fa-times';
            default:
                return 'fa-sun-o';
        }
    },
    error (message) {
        this.open({type: 'danger', content: message, title: 'Error'});
    },
    success (message) {
        this.open({type: 'success', content: message, title: 'Success'});
    },
    warning (message) {
        this.open({type: 'warning', content: message, title: 'Warning'});
    },
    info (message) {
        this.open({type: 'info', content: message, title: 'Info'});
    },
    primary (message) {
        this.open({type: 'primary', content: message, title: 'Primary'});
    },
    danger (message) {
        this.open({type: 'danger', content: message, title: 'Danger'});
    }
};
