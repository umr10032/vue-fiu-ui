/* eslint-disable no-undef */

function ElementEvent (e) {
    this.srcEvent = e;
}

ElementEvent.prototype.prevent = function () {
    if ($.loserIE()) {
        this.srcEvent.returnValue = false;
    } else {
        this.srcEvent.preventDefault();
    }
};

ElementEvent.prototype.stop = function () {
    if ($.loserIE()) {
        this.srcEvent.cancelBubble = true;
    } else {
        this.srcEvent.stopPropagation();
    }
};

ElementEvent.prototype.pressKey = function () {
    return this.srcEvent.keyCode;
};
