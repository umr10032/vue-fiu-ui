/* eslint-disable no-undef */
/**
 * Created by YongXuan.Huang on 2017/4/28.
 */

// {title: '', content: ''}
export default {
    open (options, confirm, cancel) {
        let id = 'modal_id_' + Math.floor(Math.random() * 10000000);
        $('body').append(`<div class="modal fade" id="${id}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content img-responsive"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h4 class="modal-title" id="myModalLabel">${options.title}</h4></div><div class="modal-body">${options.content}</div><div class="modal-footer"><button type="button" class="btn btn-danger" data-type="cancel">取消</button><button type="button" class="btn btn-primary" data-type="confirm">确定</button></div></div></div></div>`);
        let modal = $(`#${id}`);
        modal.modal({
            keyboard: true
        });
        modal.on('hidden.bs.modal', () => {
            $('button', modal).unbind();
            modal.remove();
        });
        let me = this;
        $('button[data-type="confirm"]', modal).click(function () {
            if (confirm !== undefined) {
                confirm($(this), modal);
            } else {
                me.closeModal(modal, options);
            }
        });
        $('button[data-type="cancel"]', modal).click(function () {
            if (cancel !== undefined) {
                cancel($(this), modal);
            } else {
                me.closeModal(modal, options);
            }
        });
    },
    closeModal: (modal, options) => {
        let timeout = options.timeout > 0 ? options.timeout : 0;
        setTimeout(() => {
            modal.modal('hide');
        }, timeout);
    }
};
