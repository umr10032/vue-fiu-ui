/* eslint-disable no-undef */
/**
 * Created by YongX on 2017/5/20.
 */

export default {
    __init () {
        let id = 'image-box';
        let div = $(`#${id}`);
        if (div.length <= 0) {
            $('body').append(`<div id="${id}" class="display-flex image-box"></div>`);
        }
        return $(`#${id}`);
    },
    wind (tagger) {
        let div = this.__init();
        div.hide();
        $(tagger).click(function () {
            div.show();
            let imageUrl;
            if (this.hasAttribute('data-image-box-url')) {
                imageUrl = $(this).attr('data-image-box-url');
            } else {
                imageUrl = $(this).attr('src');
            }
            div.append(`<div class="animated bounceInUp">
                <a class="imageBox_"><i class="fa fa-close"></i></a>
                <img src="${imageUrl}">
                </div>`);
        });

        div.delegate('a', 'click', function () {
            let p = $($(this).parent());
            p.removeClass('bounceInUp');
            p.addClass('slideOutRight');
            setTimeout(function () {
                p.remove();
                div.hide();
            }, 450);
        });
    }
};
