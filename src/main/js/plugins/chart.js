/**
 * @namespace Chart
 */
var Chart = require('../../../../node_modules/chart.js/src/core/core.js')();

require('../../../../node_modules/chart.js/src/core/core.helpers')(Chart);
require('../../../../node_modules/chart.js/src/platforms/platform.js')(Chart);
require('../../../../node_modules/chart.js/src/core/core.canvasHelpers')(Chart);
require('../../../../node_modules/chart.js/src/core/core.element')(Chart);
require('../../../../node_modules/chart.js/src/core/core.plugin.js')(Chart);
require('../../../../node_modules/chart.js/src/core/core.animation')(Chart);
require('../../../../node_modules/chart.js/src/core/core.controller')(Chart);
require('../../../../node_modules/chart.js/src/core/core.datasetController')(Chart);
require('../../../../node_modules/chart.js/src/core/core.layoutService')(Chart);
require('../../../../node_modules/chart.js/src/core/core.scaleService')(Chart);
require('../../../../node_modules/chart.js/src/core/core.ticks.js')(Chart);
require('../../../../node_modules/chart.js/src/core/core.scale')(Chart);
require('../../../../node_modules/chart.js/src/core/core.interaction')(Chart);
require('../../../../node_modules/chart.js/src/core/core.tooltip')(Chart);

require('../../../../node_modules/chart.js/src/elements/element.arc')(Chart);
require('../../../../node_modules/chart.js/src/elements/element.line')(Chart);
require('../../../../node_modules/chart.js/src/elements/element.point')(Chart);
require('../../../../node_modules/chart.js/src/elements/element.rectangle')(Chart);

require('../../../../node_modules/chart.js/src/scales/scale.linearbase.js')(Chart);
require('../../../../node_modules/chart.js/src/scales/scale.category')(Chart);
require('../../../../node_modules/chart.js/src/scales/scale.linear')(Chart);
require('../../../../node_modules/chart.js/src/scales/scale.logarithmic')(Chart);
require('../../../../node_modules/chart.js/src/scales/scale.radialLinear')(Chart);
require('./vendor/scale.time')(Chart);

// Controllers must be loaded after elements
// See Chart.core.datasetController.dataElementType
require('../../../../node_modules/chart.js/src/controllers/controller.bar')(Chart);
require('../../../../node_modules/chart.js/src/controllers/controller.bubble')(Chart);
require('../../../../node_modules/chart.js/src/controllers/controller.doughnut')(Chart);
require('../../../../node_modules/chart.js/src/controllers/controller.line')(Chart);
require('../../../../node_modules/chart.js/src/controllers/controller.polarArea')(Chart);
require('../../../../node_modules/chart.js/src/controllers/controller.radar')(Chart);

require('../../../../node_modules/chart.js/src/charts/Chart.Bar')(Chart);
require('../../../../node_modules/chart.js/src/charts/Chart.Bubble')(Chart);
require('../../../../node_modules/chart.js/src/charts/Chart.Doughnut')(Chart);
require('../../../../node_modules/chart.js/src/charts/Chart.Line')(Chart);
require('../../../../node_modules/chart.js/src/charts/Chart.PolarArea')(Chart);
require('../../../../node_modules/chart.js/src/charts/Chart.Radar')(Chart);
require('../../../../node_modules/chart.js/src/charts/Chart.Scatter')(Chart);

// Loading built-it plugins
var plugins = [];

plugins.push(
    require('./plugins/plugin.filler.js')(Chart),
    require('./plugins/plugin.legend.js')(Chart),
    require('./plugins/plugin.title.js')(Chart)
);

Chart.plugins.register(plugins);

module.exports = Chart;
if (typeof window !== 'undefined') {
    window.Chart = Chart;
}
