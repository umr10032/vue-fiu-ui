/* eslint-disable no-unused-vars,no-undef */

function Carouser (el, p) {
    let $el = el;
    let params = {
        isMove: false,
        startX: 0,
        startY: 0,
        index: 0,
        maxIndex: 0,
        timeout: null
    };

    if (p === undefined) {
        p = {};
    }

    params.loop = p.loop || false;
    params.loopTime = p.timeout || 5000;
    let content = $($el.object.children[0]);
    content.on('mousedown', onTouchStart);
    $(document).on('mousemove', onTouchMove);
    $(document).on('mouseup', onTouchEnd);
    content.on('mouseout', onTouchEnd);
    init();

    function onTouchStart (e) {
        params.isMove = true;
        params.startX = getIntervalWidth(params.index) + e.pageX;
        window.el = e;
    }

    function onTouchMove (e) {
        if (params.isMove === false) return false;
        let m = e.pageX - params.startX;
        content.css('transitionDuration', '0ms');
        content.css('transform', 'translate3d(' + m + 'px,0,0)');
    }

    function onTouchEnd (e) {
        if (params.isMove === false) return;
        params.isMove = false;
        let w = -getIntervalWidth(params.index);
        content.object.style.transitionDuration = '300ms';
        content.object.style.transform = 'translate3d(' + w + 'px , 0, 0)';
        let x = Math.abs(e.pageX - params.startX);
        let contentWidth = $el.width();
        if (x > contentWidth * 0.4) {
            next();
        }
    }

    function getEl () {
        return content;
    }

    function init () {
        computeBox();
        computePagination();
        bindButton();
        addDefaultClass();
        setLoop();
    }

    function computeBox () {
        let side = $('.carousel-side', $el);
        let box = content;
        let maxWidth = 0;
        for (let i = 0; i < side.length; i++) {
            maxWidth += side[i].width() + 33;
            side[i].css('width', $el.object.offsetWidth + 'px');
        }
        box.css('width', maxWidth + 'px');
        $el.css('visibility', 'visible');
    }

    function computePagination () {
        let count = content.param('children').length;
        let pagination = $('.carousel-pagination', $el);
        if (pagination && pagination.length) {
            return;
        }
        let html = '';
        let w = $el.object.offsetWidth;
        for (let i = 0; i < count; i++) {
            html += '<a data-index="' + i + '"></a>';
            let mw = 0;
            for (let x = i; x < count; x++) {
                mw += content.param('children')[x].offsetWidth + 30;
            }
            if (mw < w) {
                params.maxIndex = i + 1;
                break;
            } else {
                params.maxIndex = count;
            }
        }
        pagination.object.innerHTML = html;
    }

    function prev () {
        params.index--;
        if (params.index < 0) {
            params.index = params.maxIndex - 1;
        }
        switchPage(params.index);
    }

    function next () {
        params.index++;
        if (params.index >= params.maxIndex) {
            params.index = 0;
        }
        switchPage(params.index);
    }

    function bindButton () {
        $('.carousel-pagination', $el).on('click', changePage);
        $('.carousel-prev', $el).on('click', prev);
        $('.carousel-next', $el).on('click', next);
    }

    function changePage (e) {
        switchPage(e.srcElement.getAttribute('data-index'));
    }

    function switchPage (index) {
        if (index >= params.maxIndex) return false;
        let w = getIntervalWidth(index);
        removeActive();
        $el.object.querySelector('.carousel-pagination > a[data-index="' + index + '"]').className = 'active';
        content.object.style.transitionDuration = '500ms';
        content.object.style.transform = 'translate3d(-' + w + 'px , 0, 0)';
        content.object.children[params.index].style.transitionDuration = '200ms';
        content.object.children[params.index].style.transform = 'scale(.3, .3)';
        setTimeout(function () {
            content.object.children[params.index].style.transform = 'scale(1, 1)';
        }, 300);
        params.index = index;
        setLoop();
    }

    function removeActive () {
        let list = [];
        list = $el.object.querySelectorAll('.carousel-pagination > a');
        if ((list && list.length) === false) return;
        for (let index in list) {
            if (index >= 0) {
                list[index].className = '';
            }
        }
    }

    function addDefaultClass () {
        setTimeout(function () {
            let el = $el.object.querySelector('.carousel-pagination > a[data-index="0"]');
            if (el !== null) {
                el.className = 'active';
            }
        }, 100);
    }

    this.update = function update () {
        unBind();
        init();
    };

    function unBind () {
        $('.carousel-pagination', $el).unbind('click', changePage);
        $('.carousel-prev', $el).unbind('click', prev);
        $('.carousel-next', $el).unbind('click', next);
    }

    function setLoop () {
        clearTime();
        if (params.loop === true) {
            params.timeout = setInterval(function () {
                next();
            }, params.loopTime);
        }
    }

    function clearTime () {
        if (params.timeout !== null) {
            clearInterval(params.timeout);
            params.timeout = null;
        }
    }

    function getIntervalWidth (index) {
        let content = getEl();
        let w = 0;
        for (let i = 0; i < index; i++) {
            w = w + content.object.children[i].offsetWidth + 30;
        }
        if (index === 0) {
            w = 0;
        }
        return w;
    }
}
