/* eslint-disable spaced-comment */
import './app/bootstrap';
import Notify from './plugins/Notify';
import Modal from './plugins/Modal';

const install = function (Vue, options) {
	if (install.installed) return false;
	Vue.prototype.$notify = Notify;
    Vue.prototype.$modal = Modal;
};

if (typeof window !== 'undefined' && window.Vue) {
	install(window.Vue);
}

module.exports = {
	version: '1.0.0',
	install
};
