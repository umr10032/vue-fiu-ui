/**
 * Created by YongX on 2017/6/17.
 */
const carousel = `
<template>
	<div class="col-lg-12">
		<div style="height: 400px">
			<carousel :options="options">
				<carousel-item>
					<div style="height: 100%; width: 100%;background-color: whitesmoke"></div>
				</carousel-item>
			</carousel>
		</div>
	</div>
</template>
<script>
	export default {
		data () {
			return {
				options: {
					pagination: '.swiper-pagination',
					nextButton: '.swiper-button-next',
					prevButton: '.swiper-button-prev',
					slidesPerView: 1,
					paginationClickable: true,
					spaceBetween: 30,
					loop: true,
					autoplay: 2500,
					autoplayDisableOnInteraction: false
				}
			};
		}
	};
</script>`;

const bottomBar = `
<template>
	<div class="col-lg-12">
		<div style="height: 400px">
			<bottom-bar :menus="menus" style="position: relative; max-width: 400px"></bottom-bar>
		</div>
	</div>
</template>

<script>
	export default {
		data () {
			return {
				menus: [
					{link: '#', icon: 'fa-home', label: '首页'},
					{link: '#', icon: 'fa-th-large', label: '分类'},
					{link: '#', label: '天生会玩', img: 'https://mcache.ifanr.cn/static/dongguan/dist/images/6fc887dc.png'},
					{link: '#', icon: 'fa-shopping-cart', label: '购物车'},
					{link: '#', icon: 'fa-user', label: '我的'}
				]
			};
		}
	};
</script>
`;

const selector = `
<template>
    <div class="col-lg-4">
        <md-selector :data="data" v-model="value" :search="search" id="selector"/>
        <div>选择：{{value}}</div>
    </div>
</template>

<script>
    export default {
        data () {
            return {
                data: [
                    {value: 12, label: '12'},
                    {value: 13, label: '13'},
                    {value: 14, label: '14'}
                ],
                value: {value: 14, label: '14'},
            };
        }
    }
</script>
`;
export {
	carousel,
	bottomBar,
    selector
};
