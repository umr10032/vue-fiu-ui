/**
 * Created by YongX on 2017/6/17.
 */
export default {
	html: `<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>vue fiu ui example</title>
	<link rel="stylesheet" type="text/css" href="http://unpkg.com/vue-fiu-ui//dist/static/vue-fiu-ui/.css">
	<script type="text/javascript" src="http://vuejs.org/js/vue.min.js"></script>
	<script type="text/javascript" src="http://unpkg.com/vue-fiu-ui/dist/static/vue-fiu-ui.min.js"></script>
</head>
<body>
<div id="app">
	<icon type="fa-user"></icon>
</div>
<script>
	new Vue({
		el: '#app',
		data: {
			visible: false
		},
		methods: {
			show: function () {
				this.visible = true;
			}
		}
	});
</script>
</body>
</html>`
};
