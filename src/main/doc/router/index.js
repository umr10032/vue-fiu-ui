/**
 * Created by YongX on 2017/6/15.
 */

import Router from 'vue-router';
// import '';
const Home = resolve => require(['../components/Home.vue'], resolve);
const Doc = resolve => require(['../components/doc/Index.vue'], resolve);
const DocIcon = resolve => require(['../components/doc/DocIcon.vue'], resolve);
const DocInstall = resolve => require(['../components/doc/DocInstall.vue'], resolve);
const DocCarousel = resolve => require(['../components/doc/DocCarousel.vue'], resolve);
const DocCarousel2 = resolve => require(['../components/doc/DocCarousel2.vue'], resolve);
const DocBottomBar = resolve => require(['../components/doc/DocBottomBar.vue'], resolve);
const DocPanel = resolve => require(['../components/doc/DocPanel.vue'], resolve);
const DocSelector = resolve => require(['../components/doc/DocSelector.vue'], resolve);
const DocTable = resolve => require(['../components/doc/DocTable.vue'], resolve);
const DocImageBox = resolve => require(['../components/doc/DocImageBox.vue'], resolve);
const DocModal = resolve => require(['../components/doc/DocModal.vue'], resolve);
const DocEditor = resolve => require(['../components/doc/DocEditor.vue'], resolve);
const DocNotify = resolve => require(['../components/doc/DocNotify.vue'], resolve);
const DocTest = resolve => require(['../components/doc/DocTest.vue'], resolve);

export default new Router({
    mode: 'history',
    base: __dirname,
    routes: [
        {path: '/', name: '/', component: Home},
        {path: '/home', name: 'Home', component: Home},
        {path: '/doc/index', name: 'Doc', component: Doc},
        {path: '/doc/icon', name: 'Icon', component: DocIcon},
        {path: '/doc/install', name: 'Install', component: DocInstall},
        {path: '/doc/carousel', name: 'Carousel', component: DocCarousel},
        {path: '/doc/carousel2', name: 'Carousel2', component: DocCarousel2},
        {path: '/doc/bottom/bar', name: 'BottomBar', component: DocBottomBar},
        {path: '/doc/table', name: 'DocTable', component: DocTable},
        {path: '/doc/panel', name: 'DocPanel', component: DocPanel},
        {path: '/doc/selector', name: 'DocSelector', component: DocSelector},
        {path: '/doc/modal', name: 'DocModal', component: DocModal},
        {path: '/doc/editor', name: 'DocEditor', component: DocEditor},
        {path: '/doc/plugins/image/box', name: 'ImageBox', component: DocImageBox},
        {path: '/doc/plugins/notify', name: 'DocNotify', component: DocNotify},
        {path: '/doc/test', name: 'DocTest', component: DocTest},

        {path: '*', redirect: '/'}
    ]
});
