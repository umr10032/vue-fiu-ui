/* eslint-disable no-new */
/**
 * Created by YongX on 2017/6/15.
 */

import Vue from 'vue';
import VueRouter from 'vue-router';
import router from './router/index';
import App from './App.vue';
import '../../../dist/static/css/vue-fiu-ui.min.css';
import VueFiu from '../../../dist/static/js/vue-fiu-ui.min';

Vue.use(VueRouter);
Vue.use(VueFiu);

new Vue({
	el: '#app',
	router,
	render: h => h(App),
	data: {
		eventHub: new Vue()
	}
});
